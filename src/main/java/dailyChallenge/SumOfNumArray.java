package dailyChallenge;

public class SumOfNumArray {

	public static void main(String[] args) {
		int[] a = {25, 26, 9, 18, 3, 12};
		int count = 0;
		for (int i = 0; i < a.length; i++)
		{
			count = count + a[i];
		}
		System.out.println("Sum of array numbers is "+count);

	}

}
