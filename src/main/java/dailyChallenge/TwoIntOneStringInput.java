package dailyChallenge;

import java.util.Scanner;

public class TwoIntOneStringInput {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int num1 = sc.nextInt();
		int num2 = sc.nextInt();
		
		String c = sc.next();
		//int a = 10, b = 5;
		//String c = "Div";
		int d = 0;

		switch (c) {
		case "Add":
			d = num1 + num2;
			break;
		case "Sub":
			d = num1 - num2;
			break;
		case "Mult":
			d = num1 * num2;
			break;
		case "Div":
			d = num1 / num2;
			break;
		default:
			System.out.println("Invalid arithmetic operation");
			break;
		}
		System.out.println(c+" value is "+d);
	}

}
