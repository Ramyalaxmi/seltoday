package week1
.exercise1;

import java.util.Scanner;

public class LearnArray {

	public static void main(String[] args) {
		//Learn Static array
		/*Using the existing "MobilePhone" class and its methods (void sendSMS(long mobileNumber, String SMS)). 
		 Create another class with main method to send "Good morning!Happy Sunday!" SMS to the following 
		 6 mobile numbers
	9000812341
	9000812342
	9000812343
	9000812344
	9000812345
	9000812346

	//Hint: Call the method 6 times with different mobile number and same SMS
		String sms = "Good morning!Happy Sunday!";
		long mobNumber[] = {9000812341L, 9000812342L, 9000812343L, 9000812344L, 9000812345L,9000812346L};
		for (int i = 0; i<mobNumber.length; i++)
		{
			System.out.println("SMS "+i+sms+" Mobile Number "+mobNumber[i]);
		}*/
		
		//sum of odd numbers in static array
		/*int even[] = {1, 4, 5, 6, 7, 8};
		int j=0;
		for (int i =0; i<even.length; i++)
		{
			if(even[i]%2 != 0)
			{
				
				j=even[i]+j;
				System.out.println("Odd number is "+even[i]);
			}
					
	}System.out.println("Sum of odd numbers is "+j); */

		//sum of odd numbers in dynamic array
		Scanner sc = new Scanner(System.in);
		int size=sc.nextInt();
		int odd[] = new int[size];
		int j=0;
		for (int i = 0; i<odd.length; i++)
		{
			odd[i]=sc.nextInt();
			if (odd[i]%2 != 0)
			{
				j=odd[i]+j;
			}
		}
		System.out.println("Sum of odd numbers using dynamic array is "+j);
}
}
