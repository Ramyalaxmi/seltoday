package week2.excercise1;

public class Television implements Entertainment {

	public void changeChannel()
	{
		System.out.println("Changing the channel");
	}
	
	public void volumeUp() {
		System.out.println("Increasing the volume");
	}
	
	public void volumeDown() {
		System.out.println("Decreasing the volume");
	}

	@Override
	public void videosViaUSB() {
		// TODO Auto-generated method stub
		System.out.println("Play games. This is interface");
	}

	@Override
	public void playGames() {
		// TODO Auto-generated method stub
		System.out.println("Play games. This is interface");
	}

	@Override
	public void enjoyMusic() {
		// TODO Auto-generated method stub
		System.out.println("Enjoy music. This is interface");
	}
	
}
