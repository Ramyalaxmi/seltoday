package week2.excercise1;

public class SmartTV extends Television {

	public void recordVideos() {
		System.out.println("Recording the videos");
	}
	
	public void connectWIFI() {
		System.out.println("Connect to internet");
	
	}
	
	public void installAPP() {
		System.out.println("Install APPs using WIFI");
	}
	
	
	public static void main(String[] args) {
		SmartTV stv = new SmartTV();
		stv.recordVideos();
		stv.connectWIFI();
		stv.installAPP();
		stv.changeChannel();
		stv.volumeUp();
		stv.volumeDown();
		stv.playGames();
	}
	
}
