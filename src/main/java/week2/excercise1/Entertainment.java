package week2.excercise1;

public interface Entertainment {
	
	public void videosViaUSB();
	
	public void playGames();
	
	public void enjoyMusic();

}
