package wdMethods;


import java.io.IOException;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class LearnExtent {
	@Test
	public void name() throws IOException{
		

	ExtentHtmlReporter html = new ExtentHtmlReporter("./reports/result.html");
	html.setAppendExisting(true);
	ExtentReports extent = new ExtentReports();
	extent.attachReporter(html);
	
	ExtentTest logger = extent.createTest("TC002CreateLead", "Creating a lead");
	logger.assignAuthor("Ramyalaxmi");
	logger.assignCategory("Smoke testing");
	
	logger.log(Status.PASS, "The demosales Manager is logged in successfully", 
			MediaEntityBuilder.createScreenCaptureFromPath("./snaps/img1.png").build());
	
	extent.flush();
	}
	
	
}
