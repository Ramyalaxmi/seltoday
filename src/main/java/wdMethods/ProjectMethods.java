package wdMethods;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;



public class ProjectMethods extends SeMethods{
	
	@BeforeTest
	public void beforeTest() {
		System.out.println("@BeforeTest");
	}
	@BeforeClass
	public void beforeClass() {
		System.out.println("@BeforeClass");
	}
	@Parameters({"url","username","password"})
	@BeforeMethod
	public void login(String url,String username,String password) {
		beforeMethod();
		startApp("chrome", url);
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, username);
		WebElement elePassword = locateElement("password");
		type(elePassword, password);
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
		WebElement crm = locateElement("linktext", "CRM/SFA");
		click(crm);		
	}
	
	@AfterMethod
	public void closeApp() {
		closeBrowser();
	}
	@AfterClass
	public void afterClass() {
		System.out.println("@AfterClass");
	}
	@AfterTest
	public void afterTest() {
		System.out.println("@AfterTest");
	}
	@AfterSuite
	public void afterSuite() {
		endResult();
	}
	
	@BeforeSuite
	public void beforeSuite() {
		startResult();
	}

}






