package LearnExcel;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.Test;

public class learnExcelintegration {
	
	
	public static  Object[][] LearnExcel() throws IOException
	{
		//open workbook
		XSSFWorkbook wb = new XSSFWorkbook("./data/createLead.xlsx");
		
		//go to the sheet
		XSSFSheet sheet = wb.getSheetAt(0);
		
		int rowCount = sheet.getLastRowNum();
		System.out.println("Total number of rows: "+rowCount);
		
		int cellCount = sheet.getRow(0).getLastCellNum();
		System.out.println("Total cell count is "+cellCount);
		
		Object[][] data = new Object[rowCount][cellCount];
		
		for (int j = 1; j <= rowCount; j++) {
			// go to the specific row
			XSSFRow row = sheet.getRow(j);
			
			for (int i = 0; i < cellCount; i++) {
				//go to the specific cell
				XSSFCell cell = row.getCell(i);
				try
				{
				//read the string value
				String value = cell.getStringCellValue();
				System.out.println("Value is " + value);
				data[j-1][i] = value;
				}catch(NullPointerException e)
				{
					System.out.println("");
				}
			} 
		}
		wb.close();
		return data;
		
	}
	
	
	
	

}
