package week3.day2;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnTable {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://erail.in/");
		
		driver.findElementById("txtStationFrom").clear();
		driver.findElementById("txtStationFrom").sendKeys("MAS", Keys.TAB);
		driver.findElementById("txtStationTo").clear();
		driver.findElementById("txtStationTo").sendKeys("MDU", Keys.TAB);
		WebElement ele = driver.findElementById("chkSelectDateOnly");
		if (ele.isSelected()) {
			ele.click();
		}
		Thread.sleep(3000);
		
		//table
		WebElement table = driver.findElementByXPath("//table[@class = 'DataTable TrainList']");
		//number of columns
		List<WebElement> allRows = table.findElements(By.tagName("tr"));
		System.out.println(allRows.size());
		
				
				for(int i = 0; i<=allRows.size(); i++)
				{
					List<WebElement> allColumns = allRows.get(i).findElements(By.tagName("td"));
					//System.out.println(allRows.size());
					
					String text = allColumns.get(1).getText();
					System.out.println(text);
				}
				
	}	
	}


