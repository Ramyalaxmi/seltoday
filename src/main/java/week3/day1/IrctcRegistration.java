package week3.day1;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class IrctcRegistration {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		driver.findElementById("userRegistrationForm:userName").sendKeys("Test UserName");
		
		driver.findElementByLinkText("userRegistrationForm:checkavail").click();
		String text = driver.findElementById("userRegistrationForm:useravail").getText();
		System.out.println("Error message is "+text);
		
		driver.findElementById("userRegistrationForm:password").sendKeys("password");
		driver.findElementById("userRegistrationForm:confpasword").sendKeys("password"); 
		
		WebElement source = driver.findElementById("userRegistrationForm:securityQ");
		Select dd = new Select(source);
		dd.selectByVisibleText("What make was your first car or bike?");
		
		driver.findElementById("userRegistrationForm:securityAnswer").sendKeys("TVS");
		
		driver.findElementById("userRegistrationForm:firstName").sendKeys("Test FirstName");
		//Radio button
		driver.findElementById("userRegistrationForm:gender:1").click();
		driver.findElementById("userRegistrationForm:maritalStatus:1").click();
		
		WebElement source1 = driver.findElementById("userRegistrationForm:dobDay");
		Select dd1 = new Select(source1);
		dd1.selectByVisibleText("12");
		
		WebElement source2 = driver.findElementById("userRegistrationForm:dobMonth");
		Select dd2 = new Select(source2);
		dd2.selectByVisibleText("AUG");
		
		WebElement source3 = driver.findElementById("userRegistrationForm:dateOfBirth");
		Select dd3 = new Select(source3);
		dd3.selectByVisibleText("1984");
		
		WebElement source4 = driver.findElementById("userRegistrationForm:occupation");
		Select dd4 = new Select(source4);
		dd4.selectByVisibleText("Student");
		
		WebElement source5 = driver.findElementById("userRegistrationForm:countries");
		Select dd5 = new Select(source5);
		dd5.selectByVisibleText("India");
		
		driver.findElementById("userRegistrationForm:address").sendKeys("Plot number: 54");
		
		//PIN code with TAB
		//WebElement source6 = driver.findElementById("userRegistrationForm:pincode");
		//source6.sendKeys("600113");
		//source6.sendKeys(Keys.TAB);
		//Thread.sleep(5000);
		
		driver.findElementById("userRegistrationForm:pincode").sendKeys("600113", Keys.TAB);
		Thread.sleep(5000);
		
		WebElement source7 = driver.findElementById("userRegistrationForm:cityName");
		Select dd7 = new Select(source7);
		dd7.selectByVisibleText("Chennai");
		Thread.sleep(5000);
		
		WebElement source8 = driver.findElementById("userRegistrationForm:postofficeName");
		Select dd8 = new Select(source8);
		dd8.selectByIndex(1);
		Thread.sleep(5000);
		
		driver.findElementById("userRegistrationForm:landline").sendKeys("1234567890");
		System.out.println(driver.findElementById("userRegistrationForm:resAndOff:1").isSelected());
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		


	}

}
