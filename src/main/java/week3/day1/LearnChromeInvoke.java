package week3.day1;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class LearnChromeInvoke {

	public static void main(String[] args) {
		//invoke chrome browser
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		//maximize window
		driver.manage().window().maximize();
		//load URL
		driver.get("http://leaftaps.com/opentaps/control/main");
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByPartialLinkText("CRM/SFA").click();
		driver.findElementByPartialLinkText("Create Lead").click();
		driver.findElementById("createLeadForm_companyName").sendKeys("Lionbridge Technologies");
		driver.findElementById("createLeadForm_firstName").sendKeys("Ramya");
		driver.findElementById("createLeadForm_lastName").sendKeys("Kannan");
		WebElement source = driver.findElementById("createLeadForm_dataSourceId");
		Select dd = new Select(source);
		dd.selectByVisibleText("Public Relations");
		WebElement market = driver.findElementById("createLeadForm_marketingCampaignId");
		Select dd1 = new Select(market);
		dd1.selectByValue("CATRQ_AUTOMOBILE");
		WebElement industry = driver.findElementById("createLeadForm_industryEnumId");
		Select dd2 = new Select(industry);
		
		List <WebElement> allOptions = dd2.getOptions();
		for (WebElement eachOption : allOptions) {
			
		if(eachOption.getText().startsWith("M")) {
		
		System.out.println(eachOption.getText());
		}
		}
		
		
		
		
		
		
		//driver.findElementByName("submitButton").click();
		
		
		

	}

}
