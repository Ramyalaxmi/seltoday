package week3.day1;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class IrctcLinks {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		//maximize window
		driver.manage().window().maximize();
		//load URL
		driver.get("https://www.irctctourism.com/tourpkgs");
		List <WebElement> allLinks = driver.findElementsByTagName("a");
		int size = allLinks.size();
		System.out.println("Number of links available in IRCTC login page is "+size);
		for (WebElement eachoption : allLinks) {
			System.out.println(eachoption.getText());
		}
		

	}

}
